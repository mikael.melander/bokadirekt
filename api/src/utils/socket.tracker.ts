/*
Just to track and notify about active sockets
Just for fun.
 */
const activeSockets: Array<string> = [];
export default {
    addSocket: function (id: string) {
        activeSockets.push(id);
        console.log(`user connected: ${id}, total sockets: ${activeSockets.length}`);
    },
    disconnected: function (id: string) {
        let index = activeSockets.indexOf(id);
        if (index > -1) {
            console.log(`user disconnected: ${id}, total sockets: ${activeSockets.length}`);
            activeSockets.splice(index, 1);
        }
    },
    reconnectSocket: function (id: string) {
        let index = activeSockets.indexOf(id);
        if (index == -1) {
            console.log(`user reconnected: ${id}`);
            activeSockets.push(id);
        }
    }
};