import {SearchEntry} from "./SearchEntry.model";

export class Result {
    totalHits: number;
    totalDocuments: number;
    results: Array<SearchEntry>


    constructor(totalHits: number, totalDocuments: number, results: Array<SearchEntry>) {
        this.totalHits = totalHits;
        this.totalDocuments = totalDocuments;
        this.results = results;
    }
}