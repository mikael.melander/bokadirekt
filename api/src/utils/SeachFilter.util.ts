import {Result} from "../../../models/Result.model";
import {LatLng} from "../../../models/LatLng.model";
import {Entry} from "../../../models/Entry.model";
import {SearchEntry} from "../../../models/SearchEntry.model";
import calcCrow from "./calc";

// Not great.
export const scoreSearch = function (entries: Array<Entry>, search: string, location: LatLng): Result {
    let s = entries.map((entry: Entry) => {
        let se = new SearchEntry(entry)
        if (location)
            se.setDistance = calcCrow(se.position, location);
        se.score = getScore(se.name, search);
        return se;
    }).filter((a) => a.score > 0);
    if (location)
        s.sort((a,b) => sortValue(a.getDistanceValue, b.getDistanceValue));
    s.sort((a,b) => sortValue(a.score, b.score));
    return new Result(s.length, entries.length, s);
}

const sortValue = function(a: number, b: number): number {
    return a < b ? 1 : -1;
}


// Starting to run out of time here :P
// So keeping it very basic
const getScore = function (name: string, search: string): number {
    let score = 0;
    let searchParts = name.toLowerCase().split(' ');
    let searchQ = search.toLowerCase().trim();
    for (let i=0; i<searchParts.length; i++) {
        // give extra points if result starts with/exact match search term
        if (i == 0 && (searchParts[i].startsWith(searchQ) || searchParts[i] === searchQ)) {
            score += 5;
        }
        score += searchParts[i].includes(searchQ) ? 5 : 0;
        score += partScore(searchQ, searchParts[i]);

    }
    return score;
}

/**
 * Checking how many chars in a row that matches
 * To give a score of "close" matches
 * @param str1, main string
 * @param str2, part string
 */
const partScore = function (str1: string, str2: string): number {
    let score = 0;
    for (let i=0; i<str1.length; i++) {
        if (str2.length >= i) {
            if (str1[i] === str2[i]) {
                score++;
            } else {
                return score > 1 ? score : 0;
            }
        } else {
            // Don't want to score on every match, at least 2 chars in a row.
            return score > 1 ? score : 0;
        }
    }
    return score;
}