export class LatLng {
    lat: number;
    lng: number;

    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }

    get valid(): Boolean {
        // very quick and basic validation;
        if (this.lat && this.lat > -90 && this.lat < 90
            && this.lng && this.lng > -180 && this.lng < 180) {
            return true;
        } else {
            return false;
        }
    }
}