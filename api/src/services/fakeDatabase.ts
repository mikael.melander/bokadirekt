const fs = require('fs');
import {Entry} from "../../../models/Entry.model";

let json = fs.readFileSync(__dirname + '/../assets/data.json');
const database = JSON.parse(json).map((data: any) => new Entry(data));

module.exports = {
    getAll: function (): Promise<Array<Entry>> {
        return new Promise<any>(((resolve, reject) => {
            resolve(database);
        }))
    },
    // not sure why i would every use this, but seems logical to have in a fake db :P
    getById: function (id: number): Promise<Entry> {
        return new Promise<any>(((resolve, reject) => {
            resolve(null);
        }))
    },
    getByQuery: function (query: any): Promise<Array<any>> {
        return new Promise<any>(((resolve, reject) => {
            resolve(database);
        }))
    },
};
