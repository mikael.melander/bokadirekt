# Code test boka direkt
#### By Mikael Melander


### Basic idea:

* **API**: Node express *( typescript )*
* **CLIENT**: vue *( typescript )*

Create a autocomplete search with a socket io communication between api and client.


### Notes:

My plan was to keep track of the time from the commits, didn't really work properly since I got interrupted more than expected.
But it was over 2h as the assignment state, since I got a bit over ambitious.
Had to cut a couple of corners to save on time.


## Run

npm run serve in both API & CLIENT folders.
* Client expected to run at port 8080
* Api expected to run at port3000 