import {Entry} from "./Entry.model";
import {LatLng} from "./LatLng.model";
import calcCrow from "../api/src/utils/calc";

export class SearchEntry extends Entry {
    private _distance: number;
    distance: string;
    score: number;

    constructor(json: any) {
        super(json);
        // Not pretty
        this.distance = json.distance;
        this.score = json.score;
        this._distance = 0;
    }


    get getDistanceValue(): number {
        return this._distance;
    }

    set setDistance(value: number) {
        this._distance = value;
        // very very basic.
        if (value > 1000)
            this.distance = (value/1000).toFixed(2) + 'km';
        else {
            this.distance = value + 'm'
        }
    }
}