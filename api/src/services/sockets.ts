import socketTracker from "../utils/socket.tracker";
import {SOCKET_EVENTS} from "../utils/constants";
const database = require('./fakeDatabase');
import {Entry} from "../../../models/Entry.model";
import {scoreSearch} from "../utils/SeachFilter.util";

module.exports = {
    init: function (io: any) {
        io.on('connection', function (socket: any) {
            socketTracker.addSocket(socket.id);
            socket.on(SOCKET_EVENTS.GET_SEARCH, (data: any) => {
                database.getByQuery({}).then((res: Entry[]) => {
                    const result = scoreSearch(res, data.search, data.location);
                    socket.emit(SOCKET_EVENTS.RESP_SEARCH, result);
                }).catch((e: any) => {
                    // Should have error-handling. lets see if there is time (:
                    console.log(e);
                });
            });


            // Tracking events.
            socket.on('disconnect', () => {
                socketTracker.disconnected(socket.id);
            });
            socket.on('reconnect', () => {
                socketTracker.reconnectSocket(socket.id);
            });
        });

    }
}