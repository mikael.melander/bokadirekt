import {LatLng} from './LatLng.model';

export class Entry {
    id: string;
    name: string;
    position: LatLng;


    constructor(json: any) {
        this.id = json.id;
        this.name = json.name;
        this.position = new LatLng(json.position?.lat, json.position?.lng);
    }

}