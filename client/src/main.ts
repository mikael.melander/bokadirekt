import Vue from 'vue';
import VueSocketio from 'vue-socket.io-extended';
import $socket from './services/socket.instance';
import App from './App.vue';
import router from './router';
import store from './store';
import CONSTANTS from './utils/constants';

Vue.config.productionTip = false;

Vue.use(VueSocketio, $socket);

new Vue({
  router,
  store,
  render: (h) => h(App),
  sockets: {
    connect() {
      console.log('socket connected');
    },
  },
  mounted() {
    this.$store.dispatch(CONSTANTS.store.USER_LOCATION_REQUEST);
  },
}).$mount('#app');
