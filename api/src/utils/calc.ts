import {LatLng} from '../../../models/LatLng.model';

// Take from
// https://stackoverflow.com/a/18883819/12208709
// Small modifications.
export const calcCrow = function(cord1: LatLng, cord2: LatLng): number
{
    const R = 6371; // km
    const dLat = toRad(cord2.lat-cord1.lat);
    const dLon = toRad(cord2.lng-cord1.lng);
    const lat1 = toRad(cord1.lng);
    const lat2 = toRad(cord1.lng);

    const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return R * c * 1000;
}

// Converts numeric degrees to radians
const toRad = function(Value: number)
{
    return Value * Math.PI / 180;
}

export default calcCrow;