const express = require('express');
const app = express();
const http = require('http').Server(app);

const io = require('socket.io')(http, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"]
    }
});
require('./services/sockets').init(io);
const port = 3000;
const server = http.listen(port, function() {
    console.log(`We are alive with the sound of bokningar at port: ${port}!`);
});