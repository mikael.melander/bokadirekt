import { io } from 'socket.io-client';
import CONSTANTS from '../utils/constants';

export default io(CONSTANTS.api_base_url);
