import Vue from 'vue';
import Vuex from 'vuex';
import CONSTANTS from '../utils/constants';
import $socket from '../services/socket.instance';
import { SearchEntry } from '../../../models/SearchEntry.model';
import { LatLng } from '../../../models/LatLng.model';

Vue.use(Vuex);

// Using one store for small project.
export default new Vuex.Store({
  state: {
    search: null,
    searchResults: [],
    currentLocation: null as any,
    searchLocation: null as any,
  },
  getters: {
    getSearchResults: (state) => state.searchResults,
    getUserLocation: (state) => state.currentLocation,
  },
  actions: {
    [CONSTANTS.store.SET_SEARCH_VARIABLE]: ({ commit, state }, searchString) => {
      commit(CONSTANTS.store.SET_SEARCH_VARIABLE, searchString);
      // We dont want to trigger on to small searches
      if (!searchString || searchString.length < 2) return;

      let loc;
      if (state.searchLocation && state.searchLocation.valid) {
        loc = state.searchLocation;
      } else if (state.currentLocation && state.currentLocation.valid) {
        loc = state.currentLocation;
      }
      const data = {
        search: searchString,
        location: loc,
      };
      $socket.emit('getSearch', data);
    },
    // Setup to listen to search updates
    [CONSTANTS.store.LISTEN_SEARCH_RESULTS]: ({ commit }) => {
      $socket.on('resultSearch', (data) => {
        commit(CONSTANTS.store.GOT_SEARCH_SUCCESS, data);
      });
    },
    [CONSTANTS.store.USER_LOCATION_REQUEST]: ({ commit }) => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          commit(CONSTANTS.store.USER_LOCATION_SUCCESS, position);
        },
        (error) => {
          console.log(CONSTANTS.store.USER_LOCATION_REQUEST, error);
        },
      );
    },
    [CONSTANTS.store.SET_SEARCH_LOCATION]: ({ commit }, searchLocation) => {
      commit(CONSTANTS.store.SET_SEARCH_LOCATION, searchLocation);
    },
  },
  mutations: {
    [CONSTANTS.store.SET_SEARCH_VARIABLE]: (state, searchV) => {
      state.search = searchV;
    },
    [CONSTANTS.store.SET_SEARCH_LOCATION]: (state, searchLoc) => {
      state.searchLocation = searchLoc;
    },
    [CONSTANTS.store.GOT_SEARCH_SUCCESS]: (state, res) => {
      state.searchResults = res.results.map((a: any) => new SearchEntry(a));
    },
    [CONSTANTS.store.USER_LOCATION_SUCCESS]: (state, position): void => {
      state.currentLocation = new LatLng(position?.coords?.latitude, position?.coords?.longitude);
    },
  },
});
