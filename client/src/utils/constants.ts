export default {
  // Skipping env:s for this quick project. Keeping it in constants.
  api_base_url: 'http://localhost:3000',
  store: {
    // SocketIo
    SET_SEARCH_VARIABLE: 'SET_SEARCH_VARIABLE',
    LISTEN_SEARCH_RESULTS: 'LISTEN_SEARCH_RESULTS',
    GOT_SEARCH_SUCCESS: 'GOT_SEARCH_SUCCESS',

    // Location
    USER_LOCATION_REQUEST: 'USER_LOCATION_REQUEST',
    USER_LOCATION_SUCCESS: 'USER_LOCATION_SUCCESS',
    SET_SEARCH_LOCATION: 'SET_SEARCH_LOCATION',
  },
};
